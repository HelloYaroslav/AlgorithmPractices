function eliminateMaximum(dist, speed) {
    const arrivalsRecords = new Map();
    dist.forEach((kilometers, monster) => {
        const time = kilometers / speed[monster];
        if (arrivalsRecords.has(time)) {
            const monsters = arrivalsRecords.get(time);
            arrivalsRecords.set(time, [...monsters, monster]);
        }
        else
            arrivalsRecords.set(time, [monster]);
    });
    const arrivals = Array.from(arrivalsRecords).sort(([time], [nextTime]) => time - nextTime);
    console.log(arrivals);
    let eliminatedmonsters = 0;
    for (let i = 0; i < arrivals.length; i++) {
        if (+i > 1 && arrivals[i][0] - arrivals[i - 1][0] < 1)
            return eliminatedmonsters;
        if (arrivals[i][1].length > 1)
            return eliminatedmonsters + 1;
        eliminatedmonsters += 1;
    }
    return eliminatedmonsters;
}
console.log(eliminateMaximum([2, 3, 4], [3, 5, 2]));
//# sourceMappingURL=monsters.js.map