export function eliminateMaximum(dist: number[], speed: number[]): number {
	let eliminatedMonsters = 0;
	const arrivals = dist
		.map((kilometers, monster) => kilometers / speed[monster])
		.sort((a, b) => a - b);
	for (let i = 0; i < arrivals.length; i++) {
		if (i !== 0 && arrivals[i] <= i) {
			return eliminatedMonsters;
		}
		eliminatedMonsters += 1;
	}
	return eliminatedMonsters;
}
