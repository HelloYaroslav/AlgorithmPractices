import { describe, it, expect } from '@jest/globals'
import { eliminateMaximum } from './monsters'

describe('eliminateMaximum', () => {
  it.each([
    [[3, 2, 4], [5, 3, 2], 1],
    [[3, 2, 4], [5, 5, 5], 1]
  ])('Monsters with Speed %i and distance %i => %i', (speed, distance, expected) => {
    expect(eliminateMaximum(speed, distance)).toBe(expected)
  })
})
