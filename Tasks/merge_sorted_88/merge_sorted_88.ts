export const merge = (
  nums1: number[],
  m: number,
  nums2: number[],
  n: number
): number[] => {
  if (m === 0) {
    nums1.splice(0, nums2.length, ...nums2);
    return nums1;
  }
  let mk = m - 1;
  let nk = n - 1;

  for (let i = m + n; nk >= 0; i--) {
    mk >= 0 && nums1[mk] > nums2[nk]
      ? nums1.splice(i - 1, 1, nums1[mk--])
      : nums1.splice(i - 1, 1, nums2[nk--]);
  }
  return nums1;
};
