import { merge } from "./merge_sorted_88";

describe("merge", () => {
  it.each([
    [[1, 2, 2, 3, 5, 6], [1, 2, 3, 0, 0, 0], 3, [2, 5, 6], 3],
    [[1], [1], 1, [], 0],
    [[1], [0], 0, [1], 1],
  ])("Merge with Speed %i and distance %i => %i", (expected, ...args) => {
    console.log(args);
    expect(merge(...args)).toEqual(expected);
  });
});
