export function candy(ratings: number[]): number {
  const ratingsCopy = [...ratings].sort();
  let candiesCount = 0;
  let currentRate = 1;

  ratingsCopy.forEach((rating, index) => {
    if (currentRate > ratingsCopy[index]) {
      candiesCount = candiesCount + currentRate;
      currentRate = currentRate + 1;
    } else {
      candiesCount = candiesCount + currentRate;
    }
  });
  return candiesCount;
}
