import { candy } from "./candy";

describe("candy", () => {
  it.each([
    [[1, 0, 2], 5],
    [[1, 2, 2], 4],
  ])("Line %i used %i candies", (ratings, expected) => {
    expect(candy(ratings)).toBe(expected);
  });
});
