const obj = {
    name: 'deeecode',
    age: 200,
    print: function() {
      function print2(): void {
        console.log(this)
      }
      
      print2()
    }
  }
  
  // obj.print()
  const obj1 = {
    name: 'deeecode',
    age: 200,
    print: () => {
      console.log(this)
    }
  }
  
  // obj1.print()

  const obj2 = {
    name: 'deeecode',
    age: 200,
    print: function() {
      console.log(this)
    }
  }
  
  obj2.print = obj2.print.bind(obj2)
  const a: {print?: () => void} = {

  }

  a.print = obj2.print


  a.print()