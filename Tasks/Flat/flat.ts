// [1,2,3, [4,5,7, [12,23, [21,11],45]], [3,4,3], [3,3,1,1, [43,4], [41], 23], 2355, 21334] -> Infinity
// .flat(Infinity) => [1,2,3,4,5,7,12,23,21,11,45,3,4,3,3,3,1,1,43,4,41, 23, 2355, 21334]

export function flat(array: any[]): number[] {
  let flattenedArray: number[] = [];
  for (let i = 0; i < array.length; i++) {
    if (Array.isArray(array[i])) {
      flattenedArray = [...flattenedArray, ...flat(array[i])];
    } else {
      flattenedArray = [...flattenedArray, array[i]];
    }
  }
  return flattenedArray;
}
