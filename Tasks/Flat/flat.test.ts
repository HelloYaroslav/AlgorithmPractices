import { describe, expect, it } from "@jest/globals";
import { flat } from "./flat";

describe("array.flat()", () => {
	it.each([
		[
			[3, [2, 4, [2]]],
			[3, 2, 4, 2],
		],
		[
			[[1, [23, [2, 3], 5], 12, []], 3, 2, 4],
			[1, 23, 2, 3, 5, 12, 3, 2, 4],
		],
	])("Monsters with Speed %i and distance %i => %i", (array, expected) => {
		expect(flat(array)).toBe(expected);
	});
});
